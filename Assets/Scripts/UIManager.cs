using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public PlayerManager playerManager;
    
    public GameObject idleUI;
    public GameObject playUI;
    public GameObject failUI;
    public GameObject winUI;

    public Slider slider;

    private float sliderChange = 1f;
    // Start is called before the first frame update
    void Start()
    {
        idleUI.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        slider.value += sliderChange * Time.deltaTime *0.5f;
        if (slider.value<=0.1f)
        {
            playerManager.Portal(false);
        }
    }

    public void GameStart()
    {
        idleUI.SetActive(false);
        playUI.SetActive(true);
    }

    public void Fail()
    {
        failUI.SetActive(true);
        playUI.SetActive(false);
    }

    public void Restart()
    {
        playerManager.Restart();
        idleUI.SetActive(true);
        failUI.SetActive(false);
        winUI.SetActive(false);
        playUI.SetActive(false);
    }

    public void Win()
    {
        playUI.SetActive(false);
        winUI.SetActive(true);
    }

    public void Slider(float value)
    {
        sliderChange = value;
    }
}
