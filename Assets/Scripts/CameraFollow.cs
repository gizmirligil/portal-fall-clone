using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player;

    private Vector3 cameraPosDiff;
    // Start is called before the first frame update
    void Start()
    {
        cameraPosDiff = player.transform.position - transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, player.transform.position-cameraPosDiff, 1f);
    }
}
