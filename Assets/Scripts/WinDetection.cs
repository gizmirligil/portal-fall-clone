using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinDetection : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
                other.gameObject.GetComponentInChildren<Animator>().SetBool("Fall", false);
                other.gameObject.GetComponentInChildren<Animator>().SetBool("Win", true);
                other.gameObject.GetComponent<PlayerManager>().playerState = PlayerManager.States.Win;
                
            
        }
    }
}
