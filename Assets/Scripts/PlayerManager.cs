using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public Transform spawnPoint;
    public bool immune = false;
    public Animator playerAnimator;
    public Transform runTarget;
    public Transform fallTarget;
    public Transform playerIndicator;
    public GameObject playerModel;
    public GameObject portalPrefab;
    public GameObject portalMovingPrefab;
    private float portalCooldown = 0;
    private bool portalOpened = false;
    public UIManager uiManager;
    public enum States
    {
        Idle,
        Running,
        Falling,
        Dead,
        Restart,
        Win
    }

    public States playerState = States.Idle;

    private GameObject portal1;
    private GameObject portal2;
    private GameObject portalMoving;

    private Vector3 portalPos;

    private Tween up;
    private Tween down;
    private Tween fall;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (playerState == States.Dead)
        {
            
            
                uiManager.Fail();

                playerState = States.Restart;


        }
        if (playerState == States.Win)
        {
            
            
            uiManager.Win();

            playerState = States.Restart;


        }
        portalCooldown -= 1f * Time.deltaTime;
        if (portalMoving != null)
        {
            portalPos = portalMoving.transform.position;
        }
        if (portalCooldown>1)
        {
            
        }
        else
        {
            if (Input.GetButtonDown("Fire1"))
            {
                switch (playerState)
                {
                    case States.Falling:
                        Portal(true);
                        portalOpened = true;
                        break;
                    case States.Running:
                    
                        break;
                    case States.Idle:
                        playerAnimator.SetBool("Win", false);
                        playerAnimator.SetBool("Restart", false);
                        playerAnimator.SetBool("Run", true);
                        playerState = States.Running;
                        StartCoroutine(Running());
                        uiManager.GameStart();
                        break;
                    case States.Dead:
                        break;
                }
            
            }

            if (Input.GetButtonUp("Fire1") && playerState == States.Falling && portalOpened)
            {
                
                Portal(false);
            }
        }
        
    }
    IEnumerator Running()
    {
        Tween run = transform.DOMove(runTarget.position, 1f, false).SetEase(Ease.Linear);
        yield return run.WaitForCompletion();
        playerAnimator.SetBool("Run", false);
        playerAnimator.SetBool("Fall", true);
        playerState = States.Falling;
        playerIndicator.parent = transform;
        fall = transform.DOMove(fallTarget.position, 2f, false).SetEase(Ease.Linear).SetSpeedBased(true).Play();
        
        // This log will happen after the tween has completed
        Debug.Log("Tween completed!");
    }



    public void Portal(bool open)
    {
       
        
        
            if (open)
            {
            
                portalMoving = Instantiate(portalMovingPrefab, transform.position, Quaternion.identity);
                portalMoving.transform.parent = transform;
                StartCoroutine(PortalMove());
                Debug.Log("Portal Open");
                uiManager.Slider(-1);
            }
            else
            {
                portalOpened = false;
                portal1 = Instantiate(portalPrefab, transform.position+(Vector3.down*0.5f), Quaternion.identity);
                portal1.transform.localScale = Vector3.zero;
                portal1.transform.DOScale(Vector3.one*0.66f, 0.2f);
                portal2 = Instantiate(portalPrefab, transform.position+portalMoving.transform.localPosition, Quaternion.identity);
                portal2.transform.localScale = Vector3.zero;
                portal2.transform.DOScale(Vector3.one*0.66f, 0.2f);
                StopAllCoroutines();
                DOTween.Kill(portalMoving);
                portalMoving.SetActive(false);
                Destroy(portalMoving);
                Debug.Log("Portal Closed");
                StartCoroutine(PortalJump());
                portalCooldown = 2;
                uiManager.Slider(1);
                
                //portal animation
            }
        
        
    }

    IEnumerator PortalMove()
    {
        if(portalMoving!=null)
        {
            Tween down = portalMoving.transform.DOLocalMoveY(-5, 1f, false).SetEase(Ease.Linear);
            yield return down.WaitForCompletion();
            Tween up = portalMoving.transform.DOLocalMoveY(0, 0.5f, false).SetEase(Ease.Linear);
            yield return up.WaitForCompletion();
            StartCoroutine(PortalMove());
            
        }
        
    }

    IEnumerator PortalJump()
    {
        immune = true;
        transform.localScale=Vector3.zero;
        fall.Kill();
        Tween portalTween = transform.DOMove(portal2.transform.position+Vector3.down, 0.5f, false).SetEase(Ease.Linear).Play();
        yield return portalTween.WaitForCompletion();
        fall = transform.DOMove(fallTarget.position, 2f, false).SetEase(Ease.Linear).SetSpeedBased(true).Play();
        transform.localScale=Vector3.one;
        immune = false;
        Tween destroyportals = portal1.transform.DOScale(Vector3.zero, 0.2f);
        portal2.transform.DOScale(Vector3.zero, 0.2f);
        yield return destroyportals.WaitForCompletion();
        Destroy(portal1);
        Destroy(portal2);
        
    }


    public void Restart()
    {
        playerIndicator.parent = null;
        playerIndicator.transform.position = new Vector3(-12.1599998f,28.3999996f,-2.5f);
        playerAnimator.SetBool("Fall", false);
        playerAnimator.SetBool("Dying", false);
        playerAnimator.SetBool("Restart", true);
        playerState = States.Idle; 
        transform.position = spawnPoint.position;
    }
}
