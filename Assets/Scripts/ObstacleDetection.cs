using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ObstacleDetection : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (other.gameObject.GetComponent<PlayerManager>().immune)
            {
                
            }
            else
            {
                other.gameObject.GetComponentInChildren<Animator>().SetBool("Dying", true);
                other.gameObject.GetComponent<PlayerManager>().playerState = PlayerManager.States.Dead;
                DOTween.KillAll();
            }
        }
    }
}
